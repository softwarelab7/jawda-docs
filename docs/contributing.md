# Development of Docs

## Get a copy of the source files
1. Make sure you have access. Contact [admin (red@shakefx.com)](mailto:red@shakefx.com) for access. The link will open a mail client to send an email.
2. Clone or update the repository
	For the first time:

	```
	git clone git@bitbucket.org:softwarelab7/jawda-docs.git
	```

	If you are updating the docs from other collaborators changes:

	```
	git pull
	```

3. Go to directory

	```
	cd jawda-docs
	```

4. Install dependencies
	1. Install virtualenv for project. You only have to do this once.
		1. Install virtualenvwrapper

			```
			sudo apt-get install virtualenvwrapper
			```

		2. Open new terminal so that your terminal will know of virtualenvwrapper.

		3. Create virtualenv for project.

			```
			mkvirtualenv jawda-docs
			```

	2. Install docs Dependencies. You only have to do this when someone changes requirements.txt file.
	
		```
		pip install -r requirements.txt
		```

5. Run the docs

	1. Initialize work env

		```
		workon jawda-docs
		```
	
	1. Run local server.

		```
		mkdocs serve
		```

	2. Go to [http://localhost:8000/](http://localhost:8000/) to check your local copy of the docs.

6. Make changes. 
	1. Checkout a branch with same name of version you want to update the docs for:

		```
		git checkout -b <version> || git checkout <version>
		```

	2. Update from collaborators.

		1. Fetch Changes from latest master.

			```
			git fetch origin
			```

		2. Merge changes to current branch

			```
			git merge --no-ff origin/master
			```

	3. Refresh the local copy of your docs while making changes to see them. Please check out this tutorial for markdown that I saw
	randomly on the internet that looks good: [http://markdowntutorial.com/](http://markdowntutorial.com/).

7. Once done:
	1. Use git to commit changes using `git gui`. If the command line complains, install via `sudo apt-get install git-gui`

	3. Push to repository.
		```
		git pull origin refs/heads/<version>:refs/heads/<version>
		```

		```
		git push origin refs/heads/<version>:refs/heads/<version>
		```


	5. Make sure the version is active at [https://readthedocs.org/projects/jawdaandjawda/versions/](https://readthedocs.org/projects/jawdaandjawda/versions/). Create an account if you haven't already and then contact [admin (red@shakefx.com)](mailto:red@shakefx.com) for access.

	6. Wait for readthedocs to build latest or manually build using the form. and check at [http://jawdaandjawda.readthedocs.org/en/latest/](http://jawdaandjawda.readthedocs.org/en/latest/) to see published chnages. You can select the version from latest page.

8. Release to public:

	1. Checkout master.

		```
		git fetch origin
		``` 
		```
		git checkout master
		```
		```
		git pull
		```

	2. Merge versions released:

		```
		git merge --no-ff origin/<version>
		```

	3. Push latest master

		```
		git push origin master
		```

	4. Create tag for same version of the site

		```
		git tag -f -a <version> -m 'description of version'
		```

	5. Release latest changes.

		```
		git push -f origin refs/tags/<version>:refs/tags/<version>
		```

	6. Wait for readthedocs to build latest or manually build using the form. and check at [http://jawdaandjawda.readthedocs.org/en/latest/](http://jawdaandjawda.readthedocs.org/en/latest/) to see published chnages.

## Docs Framework

For full documentation visit [mkdocs.org](http://mkdocs.org).

### Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
