# Welcome to JAWDA AND JAWDA Site Docs
This is the documentation on how to manage the site JAWDA AND JAWDA.


## Administration Guide

* [Getting Started](admin/login)
	* [Logging In](admin/login) <br></br>

* [Content Management](admin/content)
	* [Adding a New Banner](admin/content#adding-a-new-banner)
	* [Changing an Existing Banner](admin/content#changing-an-existing-banner)
	* [Deleting an Existing Banner](admin/content#deleting-an-existing-banner)
	* [Enabling Image Slider Option on Banners](admin/content#enabling-image-slider-option-on-banners)
	* [Add Image on Banner Container Plugin](admin/content#add-image-on-banner-container-plugin)
	* [Adding Details on Footer Links](admin/content#adding-details-on-footer-links)
	* [Changing the Link of Existing Social Media Buttons](admin/content#changing-the-link-of-existing-social-media-buttons) 
	* [Modifying the content on Text Plugin](admin/content#modifying-the-content-on-text-plugin)
	* [Styling and Writing your own HTML using CMS](admin/content#styling-and-writing-your-own-html-using-cms) <br></br>

* [User Management](admin/customer)
	* [Giving Admin Access to a User](admin/customer#giving-admin-access-to-a-user) 
	* [Viewing Customer Information](admin/customer#viewing-customer-information) <br></br>

* [Category Management](admin/category)
	* [Adding a New Category](admin/category#adding-a-new-category)
	* [Adding a New Child Category](admin/category#adding-a-new-child-category)
	* [Sorting Categories or Child Categories](admin/category#sorting-categories-or-child-categories)
	* [Editing an Existing Category or Child Category](admin/category#editing-an-existing-category-or-child-category)
	* [Viewing the Category or Child Category on Site](admin/category#viewing-the-category-or-child-category-on-site)
	* [Deleting an Existing Category or Child Category](admin/category#deleting-an-existing-category-or-child-category)
	* [How to add 'Add to Cart' Buttons on Category Page](admin/category#how-to-add-add-to-cart-buttons-on-category-page)
	* [How to add Product to new Sub-Categories](admin/category#how-to-add-product-to-new-sub-categories) <br></br>

* [Product Management](admin/product)
	* [Adding a New Product](admin/product#adding-a-new-product)
	* [Editing an Existing Product through Dashboard](admin/product#editing-an-existing-product-through-dashboard)
	* [Editing an Existing Product through Site](admin/product#editing-an-existing-product-through-site)
	* [Creating Products that has Attributes](admin/product#creating-products-that-has-attributes)
	* [Creating Products that has different Colors or Sizes](admin/product#creating-products-that-has-different-colors-or-sizes)
	* [Uploading the Thumbnail of Product Variant](admin/product#uploading-the-thumbnail-of-product-variant)
	* [Updating the Details of an Existing Product Variant](admin/product#updating-the-details-of-an-existing-product-variant)
	* [Viewing the Product on Site](admin/product#viewing-the-product-on-site)
	* [Deleting an Existing Product](admin/product#deleting-an-existing-product) 
	* [Removing Quantity Option on certain Products](admin/product#removing-quantity-option-on-certain-products) 
	* [Changing the Product Pricing](admin/product#changing-the-product-pricing) <br></br>

* [Product Type Management](admin/ptype)
	* [Adding a New Product Type](admin/ptype#adding-a-new-product-type)
	* [Editing an Existing Product Type](admin/ptype#editing-an-existing-product-type)
	* [Editing an Existing Product Type that has Option Attributes](admin/ptype#editing-an-existing-product-type-that-has-option-attributes)
	* [Deleting an Existing Product Type](admin/ptype#deleting-an-existing-product-type) <br></br>

* [Order Management](admin/order)
	* [Changing the Order Status](admin/order#changing-the-order-status)
	* [Creating New Shipping Event](admin/order#creating-new-shipping-event)
	* [Creating New Payment Event](admin/order#creating-new-payment-event) <br></br>

* [Stock Management](admin/stock)
	* [Managing Product Alerts](admin/stock#managing-product-alerts) 
	* [Managing Low Stock Alerts](admin/stock#managing-low-stock-alerts) <br></br>

* [Partners](admin/partner)
	* [Adding a New Partner](admin/partner#adding-a-new-partner) 
	* [Update the Address and Linked Users of an Existing Partner](admin/partner#update-the-address-and-linked-users-of-an-existing-partner)
	* [Link an Existing User to an Existing Partner](admin/partner#link-an-existing-user-to-an-existing-partner)
	* [Link a New User to an Existing Partner](admin/partner#link-a-new-user-to-an-existing-partner) 
	* [Delete an Existing Partner](admin/partner#delete-an-existing-partner) <br></br>
	
* [Ranges](admin/range)
	* [Creating a New Range](admin/range#creating-a-new-range)
	* [Update an Existing Range](admin/range#update-an-existing-range)
	* [Update the Products Included in Existing Range](admin/range#update-the-products-included-in-existing-range)
	* [Delete an Existing Range](admin/range#delete-an-existing-range) <br></br>

* [Promotions](admin/offer)
	* [Creating New Offer](admin/offer#creating-new-offer)
	* [Managing an Existing Offer](admin/offer#managing-an-existing-offer)
	* [Creating New Free Shipping Offer](admin/offer#creating-new-free-shipping-offer)
	* [Managing an Existing Free Shipping Offer](admin/offer#managing-an-existing-free-shipping-offer)
	* [Creating New Voucher](admin/offer#creating-new-voucher) 
	* [Managing an Existing Voucher](admin/offer#managing-an-existing-voucher) <br></br>

* [Reports](admin/report)
	* [Generating a Report](admin/report#generating-a-report)
	* [Downloading a Report](admin/report#downloading-a-report) <br></br>

* [Shop Page](admin/shop)
	* [Changing the Image of a Category Link](admin/shop#changing-the-image-of-a-category-link) <br></br>

* [Design Services Page](admin/design)
	* [Changing the Image of a Category Link](admin/design#changing-the-image-of-a-category-link)
	* [Selecting the Right Template](admin/design#selecting-the-right-template)
	* [Enabling Thumbnails and Image Preview on Project Page](admin/design#enabling-thumbnails-and-image-preview-on-project-page)
	* [Changing the Image of an Existing Thumbnail](admin/design#changing-the-image-of-an-existing-thumbnail)
	* [Adding a New Child Page](admin/design#adding-a-new-child-page)
	* [Editing an Existing Page](admin/design#editing-an-existing-page)
	* [Deleting an Existing Page](admin/design#deleting-an-existing-page)
	* [Editing an Image](admin/design#editing-an-image)
	* [Adding a Text on Our Approach or FAQ Page](admin/design#adding-a-text-on-our-approach-or-faq-page)
	* [Editing a Text on Our Approach or FAQ Page](admin/design#editing-a-text-on-our-approach-or-faq-page)
	* [Deleting a Text Entry on Our Approach or FAQ Page](admin/design#deleting-a-text-entry-on-our-approach-or-faq-page) 
	* [Change the Title of an Existing Category](admin/design#change-the-title-of-an-existing-category) <br></br>

* [Sojawda Blog Page](admin/sojawda)
	* [Adding a New Category](admin/sojawda#adding-a-new-category)
	* [Adding a New Entry](admin/sojawda#adding-a-new-entry)
	* [Viewing the List of Categories](admin/sojawda#viewing-the-list-of-categories)
	* [Viewing the List of Entries](admin/sojawda#viewing-the-list-of-entries) <br></br>

* [About Page](admin/about)
	* [Adding a New Template to Plugins](admin/about#adding-a-new-template-to-plugins)
	* [Changing the Picture on Left Plugin](admin/about#changing-the-picture-on-left-plugin)
	* [Updating the Text on Right Plugin](admin/about#updating-the-text-on-right-plugin)
	* [Deleting an Existing Template](admin/about#deleting-an-existing-template) <br></br>

* [Contact Page](admin/contact)
	* [Changing the Details on Contact Form](admin/contact#changing-the-details-on-contact-form)
	* [Changing the Details on Text Template](admin/contact#changing-the-details-on-text-template)
	* [Changing the Address on Google Map Template](admin/contact#changing-the-address-on-google-map-template)
	* [Deleting an Existing Template](admin/contact#deleting-an-existing-template) <br></br>

* [Press Page](admin/press)
	* [Adding a New Press Link](admin/press#adding-a-new-press-link)
	* [Editing an Existing Press Link](admin/press#editing-an-existing-press-link)
	* [Deleting an Existing Press Link](admin/press#deleting-an-existing-press-link) <br></br>


