
### Adding a New Press Link

1. Log in using your Admin credentials.

2. Go to Press page.

	![Press](/img/pr1.png) <br></br>

3. Click _**Live**_ status button to set your page into editing mode. Once clicked, it will turned to _**Draft**_ status.

4. Click _**Structure**_

5. _**Press Links**_ plug in will appear with an item under it named **Press Links Container Plugin 69**. Move your cursor to its menu and select _**Press Link Plugin**_.

	![Press](/img/pr2.png) <br></br>

6. _**Press Link Plugin**_ template will pop up. To create a new entry to your Press page, fill in the following required fields:
	- **Title**- title of your entry.
	- **Date Created**- date of your entry, use either the calendar icon or click **Today** if applicable.
	- **Link**- web address of the page, must a complete url.
	- **Image**- front image of your entry. <br></br>

	**Note:** If you want to view the link in a new tab or window, tick the checkbox. I suggest to enable this option to avoid shifting of pages on the same window.

	![Press](/img/pr3.png) <br></br>

7. Once you're done, click _**Save**_ to continue.

8. Newly entered press link are now added under **Press Links Container Plugin 69**. To check, just click the down arrow beside it and there you go, the list of created press links will display.

	![Press](/img/pr4.png) <br></br>

9. Click _**Publish Changes**_ to post your made changes. A message that says _"The content was successfully published"_ must show up and new entries must now display on press page, just as like the picture below.

	![Press](/img/pr9.png)  <br></br>



### Editing an Existing Press Link

1. Enable the editing mode of the site by clicking the _**Live**_ status button.

2. There are three ways to edit an existing entry on Press page, first and the simplest one is through **Content** view wherein as you hover over Press entries you'll find a text guideline that says _"double click to edit"_. Move your cursor over the entry that you wish to update and follow the instruction, double click.

	![Press](/img/pr8.png)

	The second way is by clicking the **Structure** button. Expand the items under **Press Links Container Plugin 69** by clicking the down arrow. Select the entry that you wish to update, click its menu and select **Edit**.

	![Press](/img/pr5.png)

	The third way is quite similar to the second one, click the **Structure** and expand the items under **Press Links Container Plugin 69**. Select the entry that you wish to update, then double click (a hand icon must show first before you do the double clicking). <br></br>

3. _**Press Link Plugin**_ window will pop up. This is the right time to edit and update the information of your selected entry.

	![Press](/img/pr6.png) <br></br>

4. Once done, click _**Save**_ to continue or _**Cancel**_ to return to the menu. If you made some update, don't forget to click _**Publish Changes**_ to post your made changes. <br></br>



### Deleting an Existing Press Link

1. Enable the editing mode of the site by clicking the _**Live**_ status button.

2. Click _**Structures**_.

3. From the items under **Press Links Container Plugin 69**, select the entry that you wish to delete.

4. Click its menu and select _**Delete**_.

	![Press](/img/pr7.png) <br></br>

5. A confirmation message will pop out to validate your action.

6. Click _**Yes, Im Sure**_ button to continue or _**Cancel**_ to return to menu. When _**Yes, Im Sure**_ button is clicked, hit the _**Publish Changes**_ button to update the press page and removed your selected entry. <br></br>
