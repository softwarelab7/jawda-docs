
### Adding a New Category

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

	![Category](/img/c1.png) <br></br>

3. You will be then directed to Category Management page, created categories are all listed here. To continue, click _**Create new category**_ button.

	![Category](/img/c2.png) <br></br>

4. Fill in or use all the following required fields:
	- **Name**- category title
	- **Description**- short description of category, this must consist of few words only (i.e. by Jawda and Jawda)
	- **Image**- a graphical representation of category but this won't display on the main site. To insert an image, click _**Choose File**_ button and locate your preferred photo.
	- **Use Add Basket Buttons**- this option enables you to use 'add to basket buttons' to easily add multiple items to shopping bag. Once viewed on site, you'll notice a plus (+) sign attached unto bottom right side hand of the box and when those signs were clicked, the product will be added to your basket and a mini shopping bag will pop out to display the just added items.
	- **Position**- you can manage to place your category anywhere you want by using this option. There are three available choices from the list, see the descriptio of each below:
		- **First child of**- when selected, category will be placed as the first item of chosen **Relative To**.
		- **Before**- when selected, category will be positioned before the chosen **Relative To**.
		- **After**- contrary with _Before_, category will be positioned after the chosen **Relative To**.
	- **Relative To**- regrouping of categories/child categories? No problem! <br></br>

	![Category](/img/c3.png) <br></br>

5. Click _**Save**_ to finish or click _**Cancel**_ to void the change. Newly created category must now added to Categories list. <br></br>


### Adding a New Child Category

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

	![Category](/img/c1.png) <br></br>

3. From the menu, select one category that you wish to obtain a child category. Click **Actions** then select _**Add child category**_.

	![Child-Category](/img/cc1.png) <br></br>

4. Same with creating a new category, fill in or use all the following required fields:
	- **Name**- child category title
	- **Description**- short description of your child category, this must consist of few words only (i.e. by Jawda and Jawda)
	- **Image**- a graphical representation of child category, this time it will be displayed on the main site when its mother category is clicked . To insert an image, click _**Choose File**_ button and locate your preferred photo.
	- **Use Add Basket Buttons**- this option enables you to use 'add to basket buttons' to easily add multiple items to shopping bag.
	- **Position**- this option enables you to place your child categories somewhere under its mother category. There are three available choices from the list, see the descriptio of each below:
		- **First child of**- when selected, child category will be placed as the first item of default **Relative To**.
		- **Before**- when selected, child category will be positioned before the default **Relative To**.
		- **After**- contrary with _Before_, child category will be positioned after the default **Relative To**.
	- **Relative To**- when you select the **Add child category** as what instructed in #3 then this field is pre-filled, leave it as it is. <br></br>

5. Click _**Save**_ to finish or click _**Cancel**_ to void the change. Newly created child category must now be added to Categories list under its parent. <br></br>



### Sorting Categories or Child Categories

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

3. From the menu, select the category or child category that you want to reposition. Drag the item (you must see the broken line before you can move the item) to your target place and drop it. Dragged item should look like this:

	![Sort](/img/sc1.png)

Observe the following guideline on dropping the dragged item:

- To relocate, drag and drop the item unto line with circle on its left tip;

	![Sort](/img/sc2.png) <br></br>

- To nest, drag and drop the item unto bordered item.

	![Sort](/img/sc3.png)

	**Note:** While sorting, some categories with child category sometimes tend to unshow its border line when its sub categories are fully displayed, it is difficult to nest when those borders are not shown. To avoid this, always unexpand or hide the properties of a parent by clicking the small black arrow.

	![Sort](/img/sc4.png) <br></br>



### Editing an Existing Category or Child Category

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

	![Category](/img/c1.png) <br></br>

3. From the menu, select one category or child category that you wish to update. Click **Actions**, then select _**Edit**_.

	![Edit](/img/ec1.png) <br></br>


4. Just like creating a category or child category, same form will display. Go to section that you wish to edit, you may review again the description of each field below:
	- **Name**- category title
	- **Description**- short description of category, this must consist of few words only (i.e. by Jawda and Jawda)
	- **Image**- a graphical representation of category that displays on Shop main page. To insert an image, click _**Choose File**_ button and locate your preferred photo.
	- **Use Add Basket Buttons**- this option enables you to use 'add to basket buttons' to easily add multiple items to shopping bag. Once viewed on site, you'll notice a plus (+) sign attached unto bottom right side hand of the box and when those signs were clicked, the product will be added to your basket and a mini shopping bag will pop out to display the just added items.
	- **Position**- you can manage to place your category anywhere you want by using this option. There are three available choices from the list, see the descriptio of each below:
		- **First child of**- when selected, category will be placed as the first item of chosen **Relative To**.
		- **Before**- when selected, category will be positioned before the chosen **Relative To**.
		- **After**- contrary with _Before_, category will be positioned after the chosen **Relative To**.
	- **Relative To**- regrouping of categories/child categories happens here. <br></br>


5. Click _**Save**_ to finish or _**Cancel**_ to return to the menu. Newly updated information must now be visible in the list and on the main site. <br></br>




### Viewing the Category or Child Category on Site

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

3. From the menu, select the category that you wanted to see on site. Just click **Actions**, then select _**View on site**_.

4. You will be then directed to main site. At this point, I chose to view the Jawline Collection category, this page should appear afterwards:

	![View](/img/view.png) <br></br>



### Deleting an Existing Category or Child Category

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

	![Category](/img/c1.png) <br></br>

3. From the menu, choose the category or child category that you wish to delete. Click **Actions**, then select _**Delete**_.

	![Delete](/img/dc1.png) <br></br>

4. A delete confirmation message will come out to validate your action.

5. Click _**Delete**_ to continue or _**Cancel**_ to return to the menu. As you clicked the Delete option, concerned category must now removed from the Categories list and must no longer displayed on the main site. <br></br>



### How to add 'Add to Cart' Buttons on Category Page

If you wanted to add an item to basket without visiting its main page then you must learn to enable the **Use Add Basket Buttons** option. To locate the said option, simply follow these instructions:

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

3. From here, you can either click the **Create New Category** button or edit an existing category or child category by clicking its **Actions**, select _**Edit**_.

	![cart](/img/cart1.png) <br></br>

4. Either way will lead you to Category model, toggle **Use Add Basket Buttons** and click Save.

	![cart](/img/cart4.png) <br></br>

	To check if 'add to cart' button were added, locate the category that you just update, click **Actions** and select _**View on Site**_.

	![cart](/img/cart2.png) <br></br>

	A plus(+) icon should now be added unto bottom right side hand of the image, just like this..

	![cart](/img/cart3.png) <br></br>



### How to Add Product to New Sub-Categories

1. Log in using Admin credentials.

2. Go to **Shop** menu and find the product that you want to be added to newly created sub-categories.

3. From product page, click **Edit this Product** link.

	![sub](/img/sub1.png) <br></br>

4. Under **Product Category**, select from the list which category you want to place your product and then click **Save**. As an example, I transferred the _**Stallion**_ card to **Birthday** sub-category.

	![sub](/img/sub2.png) <br></br>

5. A confirmation message will appear, click **View it on the site** button to check the update. _**Stallion**_ should now be under **Birthday** sub category.

	![sub](/img/sub3.png) <br></br>
