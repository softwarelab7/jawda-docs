
### Logging In

* Go to JAWDA AND JAWDA site.

* Click _**Sign In**_.

	![Sign In](/img/h1.png) <br></br>

* Enter Admin credentials and click _**Log In**_.

  	![Log In](/img/h2.png)