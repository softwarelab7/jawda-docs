

### Adding a New Banner

The only way to manage your banner is through django CMS, a content management system platform linked to this site that makes the administrator's job more quick and simple. To get started, observe the following guidelines:

1. Always log in using your Admin credentials.

2. Go to homepage, quickest way to do that is by clicking the JAWDA AND JAWDA logo.

	![banner](/img/b1.png) <br></br>

3. Enable the editing mode of the page, there are two ways to do that:

	- First and simplest way is by clicking the _**Live**_ status button at the top right hand of the page. Once clicked, it will turned to _**Draft**_ status.

 	![banner](/img/b2.png) <br></br>

 	- Second is through _**Page**_ menu at the top of the logo, click the said menu and select _**Edit this Page**_ from the drop down list.

	![banner](/img/b3.png) <br></br>

4. You'll find two distinct buttons named **Structure** (back end framework of the page) and **Content** (user facing page), click _**Structure**_ to show the admin platform.

 	![banner](/img/b4.png) <br></br>

5. From there, look for _**Banners**_ plugin, click its menu and look for _**Picture**_. A new window will then come out.

 	![banner](/img/b5.png) <br></br>

 	![banner](/img/b6.png) <br></br>

6. To upload an image click _**Choose file**_ and locate the picture that you want to display as your banner, click _**Save**_ afterwards.

	![banner](/img/b7.png) <br></br>

7. To post the made changes on main site, click _**Publish Changes**_. Once clicked, notice that page status is now back to _**Live**_, a confirmation message will prompt and voila the image is now posted.

	![banner](/img/b10.png) <br></br>

	![banner](/img/b11.png) <br></br>



### Changing an Existing Banner

1. Log in using Admin credentials.

2. After you signed in, go to homepage, quickest way to do that is by clicking the JAWDA AND JAWDA logo.

3. Choose from two different ways of editing the page, either way will lead you to same page. See step 3 of [Adding a new Banner](./content.md#adding-a-new-banner).

4. Click _**Structure**_.

5. Click the menu of the uploaded image and select _**Edit**_.

	![banner](/img/b8.png) <br></br>

	![banner](/img/b9.png) <br></br>

6. _**Picture**_ window will come out, change the existing banner by clicking _**Choose file**_ , locate and upload your preferred new image and click _**Save**_. One other way to change the current banner is by deleting completely the existing one (Banner > Existing Image > Menu > Delete) and replace it with a new one through **Banners** menu, follow the steps 5 & 6 from [Adding a new Banner](./content.md#adding-a-new-banner).

7. Make sure to click the _**Publish Changes**_ button to post the made changes. <br></br>



### Deleting an Existing Banner

1. Log in using Admin credentials.

2. After you signed in, go to homepage, quickest way to do that is by clicking the JAWDA AND JAWDA logo.

3. Turn your page into editing mode, click _**Live**_ status button located at the top right hand of the page. Status will turned to **Draft** after, that means page is now ready to be modified.

	![banner](/img/b3.png) <br></br>

4. Click _**Structure**_.

5. Click the menu of the uploaded image and select _**Delete**_.

	![banner](/img/b12.png) <br></br>

6. A confirmation message will pop out to validate your action.

7. Click _**Yes, Im Sure**_ button to continue or _**Cancel**_ to return to plugin menu. When _**Yes, Im Sure**_ button is clicked, hit the _**Publish Changes**_ button to post the made changes. <br></br>



### Enabling Image Slider Option on Banners

Site has this option called Image Carousel or Image Slider (sometimes called 'rotating offers'). This option was normally used to display different set of offers in homepage alone. Like a merry go round, this option continously rotates and display the images that you inserted on the plugin. To enable this option, refer to the following procedures:

1. Make sure to log in using your Admin credentials.

2. Go to homepage, to do that simply click the Jawda and Jawda logo.

3. Click the **Live** status button located at the upper right hand of the page. As you clicked this button, **Live** status will turn to **Draft** and two buttons such as **Structure** and **Content** will appear beside it.

	![banner](/img/ca1.png)

	![banner](/img/ca2.png) <br></br>

4. Click **Structure** to see the back end side of the site. On **Banners**, click the menu and look for **Banner Container Plugin**. Select this plugin, its window will pop out with a message that says 'there are no further settings for this plugin'.

	![banner](/img/ca3.png) <br></br>

5. To finish, click the **Save** button. After you saved your action, you'll notice that the plugin is now displayed under **Banners** and it is now ready to display your images like a slideshow. To learn how to add images on this plugin, refer to the next section.

	![banner](/img/ca4.png) <br></br>



### Add Image on Banner Container Plugin

You learned how to enable the **Banner Container Plugin** earlier, now is the time to experience adding image on it. These images are the pictures that will display and flashes in your homepage. To start adding image, follow the steps below:

1. Log in using your Admin credentials.

2. Same with what we did in enabling the **Banner Container Plugin**, go to homepage (simply click the Jawda and Jawda logo) and click the **Live** status button.

	![banner](/img/ca1.png) <br></br>

3. Click the **Structure** button and from **Banner Container Plugin** menu, locate the **Banner Plugin**.

	![banner](/img/ca5.png) <br></br>

4. Select **Banner Plugin** and wait til its window displays. Click **Choose File** and from then select the image that you want to display on your banner and click **Save**.

	![banner](/img/ca6.png) <br></br>

	Wait until the page finish its loading and voila, image is now saved under **Banner Container Plugin**. To add more image, follow again the steps 3 & 4.

	![banner](/img/ca7.png) <br></br>

5. To finish, click **Publish Changes** button (the blue one) and this will display your changes on the main site. This feature will automatically display each of your uploaded images like a auto slideshow, or you can click the small dots (see picture below, highlighted in red) under the banner image to manually navigate each pictures.

	![banner](/img/ca8.png) <br></br>



### Adding Details on Footer Links

1. Log in using Admin credentials.

2. Click _**Live**_ to edit the page and click one of the 3 footer links located at the bottom left hand of the page: Returns, Privacy Policy and Orders and Shipping. As an example, select _**Returns**_.

3. You should be now directed to Return page, to check that observe the address of the page, it should be now _jawda-staging.softwarelab7.com/returns_. It is blank page because you haven't enter any values yet.

4. Click _**Structures**_ and locate _**Main**_ plugin.

	![footer](/img/f0.png)<br></br>

5. Hover to its menu and select _**Text**_ template. On this template you can add text or image to display the content of your page.

	![footer](/img/f1.png)<br></br>

	Use _**Source**_ button to style your page using html, see the example below. I added a margin so that the text would display with border on each sides. You may also check [Styling and Writing your own HTML using CMS](./content.md#styling-and-writing-your-own-html-using-cms) to learn more about HTML.

	![footer](/img/f2.png)<br></br>

6. Click _**Save**_ and then publish the made changes. <br></br>



### Changing the Link of Existing Social Media Buttons

1. Log in using Admin credentials.

2. Click _**Live**_ to edit the page and then hit _**Structures**_.

3. Locate _**Social Buttons**_ plugin.

	![social](/img/sm1.png)<br></br>

4. Hover over _**Social Buttons Plugin 40**_ menu and select **Edit**.

	![social](/img/sm2.png)<br></br>

5. _**Social Button Plugin**_ will pop out with current value or link per social media account. To change the link, simply override the new link to any of the four existing social media fields.

	![social](/img/sm3.png)<br></br>

6. Click _**Save**_ and then publish the made changes. <br></br>



### Modifying the content on Text Plugin

The text plugin that you can find on CMS has different functions that you can use to modify the content that you made. These functions are located at the upper part of the **Text Plugin** window. Function icons and its usages are listed below:

![text](/img/t1.png) **Undo** & **Redo**- this will undo or redo your last action. <br></br>
![text](/img/t2.png) **CMS Plugins**- allows you to ad any of the given plugins (file, link and picture). <br></br>
![text](/img/t0.png) **Font Size**- this allows you to change the font size for selected text. <br></br>
![text](/img/t3.png) **Paragraph Format**- allows you to select a header level to apply to the text or simply select **Normal** as its default. <br></br>
![text](/img/t4.png) **Formatting Styles**- provides list of style options to apply on your content, **Lower case** format is available on this section. <br></br>
![text](/img/t5.png) **Text Color**- allows you to change the text color for the text you've selected. <br></br>
![text](/img/t6.png) **Background Color**- allows you to apply a color background to selected text. <br></br>
![text](/img/t7.png) **Paste as Plain Text**- this allows you to paste text as plain text. <br></br>
![text](/img/t8.png) **Paste from Word**- this allows you to paste content from Microsoft Word. <br></br>
![text](/img/t9.png) **Bold**- this applies bold styles to selected text. <br></br>
![text](/img/t10.png) **Italics**- this applies italic styles to selected text. <br></br>
![text](/img/t11.png) **Underline**- this applies underline style to selected text. <br></br>
![text](/img/t12.png) **Subscript**- sets the text to be a subscript of other text. <br></br>
![text](/img/t13.png) **Superscript**- sets the text to be a superscript of other text. <br></br>
![text](/img/t14.png) **Align Left**- left justify the selected text. <br></br>
![text](/img/t15.png) **Center**- center justify the selected text. <br></br>
![text](/img/t16.png) **Align Right**- right justify the selected text. <br></br>
![text](/img/t17.png) **Insert Horizontal Line**- allows you to create a horizontal line across your entire content. <br></br>
![text](/img/t18.png) **Insert/Remove Numbered List**- adds or removes numbered list to your page. <br></br>
![text](/img/t19.png) **Insert/Remove Bulleted List**- adds or removes bulleted list to your page. <br></br>
![text](/img/t20.png) **Decrease Indent**- decrease the indent of selected content. <br></br>
![text](/img/t21.png) **Increase Indent**- increase the indent of selected content. <br></br>
![text](/img/t22.png) **Table**- allows you to enter table to your page. <br></br>



### Styling and Writing your own HTML using CMS


One other way to improve the look of your page is by using HTML. HTML or Hypertext Markup Language has a big role in modifying the elements of a site. It consists series of short codes that are called **tags**. Tags are the word between the angle brackets that normally come in pairs, the start tag `<tag>`and the end tag `</tag>`.

Simplest way to style a page is by adding the **style=" "** attribute to an html just like `<div>` tag. `<div>` represents division or section of a page. Common way to add one style property attached to a `<div>` tag looks like this:
	`<div style="property:value;"> </div>`

For example, you want the size of your text to be 10px, the attribute would look like this:
	`<div style="font-size: 10px;">text</div>`

You may also apply more than one property in the style attribute. Simply place a semicolon (;) after your first property and value, then add more property and values every after its semicolons. Let's say you want your text to be 10px and italic, we would write the following:
	`<div style="font-size: 10px; font-style: italic">text</div>`


There are plenty of style property that are very useful in CMS (**Text** only) and they are as follows:

| Property      | Description                     | Possible Values         | Example                  |
|:--------------|:--------------------------------|:------------------------|:-------------------------|
| `font-style` | declares the font style | _normal_ <br /> _italic_ <br /> _oblique_ |`<div style="font-style: italic;"></div>`|
| `font-weight` | declares the font weight, sets how thick or thin characters in text should be displayed | _normal_ <br /> _bold_ <br /> _bolder_ <br /> _lighter_ | `<div style="font-weight: bold;"></div>`|
| `font-size` | declares the size of <br /> the font | refers to lengths (example: _6px_, _8px_, _10px_) | `<div style="font-size: 12px;"></div>`|
| `color` | defines the text color <br /> to be used | name color: _red_, _blue_, _purple_ | `<div style="color: yellow;"></div>` |
| `text-align` | specifies the <br /> alignment of text in <br /> an element | _left_ <br /> _right_ <br /> _center_ <br /> _justify_ | `<div style="text-align: left;"></div>` |
| `margin` | defines the space around elements | specify a margin in px: _12px_, _8px_ | `<div style="margin-top: 5px;"></div>` |

<br />

Aside from style property, you should be familiar with some helpful HTML tags.

| Tag | Definition | Usage |
|:----|:-----------|:------|
| `<p>` | defines a paragraph | automatically add some space or <br /> margin before and after each `<p>` |
| `<h1>` to `<h6>` | heading, `<h1>` creates the largest <br /> headline  while `<h6>` creates the smallest | for headings only |
| `<b>` | creates bold text | emphasizes a word (bold) |
| `<i>` | creates italic text | emphasizes a word (italic) |
| `<ol>` | creates a ordered list, this can be <br /> numerical or alphabetical | works with `<li>` tag |
| `<ul>` | creates a bulleted list | use `<ul>` together with `<li>` <br />  to create unordered list |
| `<li>` | defines a list item | used in ordered list `<ol>` and unordered list `<ul>` |

<br></br>