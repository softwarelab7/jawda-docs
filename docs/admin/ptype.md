
### Adding a New Product Type

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ and select _**Product Types**_.

	![Catalogue](/img/pt1.png) <br></br>

3. You'll be directed to **Product Type Management** page wherein all created product types were listed (if any). To continue, click _**Create new product type**_ button.

	![Product Type](/img/pt2.png) <br></br>

4. Fill in the **Name** field and toggle the given option if necessary. See description of each below:
	- **Name**- name of product type (i.e. Apparel, Shoes)
	- **Requires Shipping?**- applies to items that are meant to be delivered like furniture, paintings and other similar items. Products that doesn't require this option are only those under digital download category like music, e-greeting cards.
	- **Track Stock Levels?**- when enabled, you're allowing all the items under such product type to monitor its inventory that can be viewed on **Stock Alerts** table, as what discussed on [Managing Stock Alerts](). <br></br>

	![Type Info](/img/pt3.png) <br></br>

5. Click _**Save**_ to finish or _**Cancel**_ to return to previous page. <br></br>



### Editing an Existing Product Type

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ and select _**Product Types**_.

	![Catalogue](/img/pt1.png) <br></br>

3. From the menu, select the product type that you wish to update. Click **Actions** and select _**Edit product type**_.

	![Edit](/img/ept1.png) <br></br>

4. Go to section that you wish to edit.

5. Click _**Save**_ to finish. <br></br>



### Editing an Existing Product Type that has Option Attributes

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ and select _**Product Types**_.

3. From the menu, select the product type that you wish to update its option attribute. Click **Actions** and select _**Edit product type option attributes**_.

	![Edit](/img/ptoa1.png) <br></br>

4. Fill in _**Option Group**_ and its _**Options**_. You can add a thumbnail or image per option by clicking _**Choose File**_ button and a new window will come out, locate the image and then upload it afterwards. Be advised that **Delete** (trash icon) and **Add** (plus sign icon) buttons are present on this page.

	- _**Option Group**_- product attribute title. (ex. Fabric, Finish)
	- _**Option**_- value of created option group. (ex. Cotton, Charcoal) <br></br>

	![Edit-PTOA2](/img/ptoa2.png)

	**Note:** Products under these product types (with present attribute) will automatically inherit the attributes from the time of creation. <br></br>

5. Click _**Submit**_ to Save. <br></br>



### Deleting an Existing Product Type

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ and select _**Product Types**_.

3. From the menu, select the product type that you wish to remove from your system. Click **Actions** and select _**Delete**_.

	![Delete](/img/dpt1.png) <br></br>

4. A delete confirmation message will come out to validate your action. Here's what it looks like:

	![Delete](/img/dpt2.png) <br></br>

5. Click _**Delete**_ to continue or _**Cancel**_ to return to the menu.

**Note:** Please be advised that you won't be able to delete a certain product type if there are still active products associated to it. Deletion is only possible to those product types that wasn't linked to a product.
