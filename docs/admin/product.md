
### Adding a New Product

1. Log in using Admin credentials.

2. After you signed in, click _**Dashboard**_.

3. Click _**Catalogue**_ and select _**Products**_.

	![Catalogue](/img/p2.png) <br></br>

4. You will be then directed to **Product Management** page. Go to **Create New Product** section and choose one product type. If there is none, follow the guideline in creating new product type, see [Adding a new Product Type](./ptype.md#adding-a-new-product-type).

	![Product-Type](/img/p3.png) <br></br>

5. After selecting product type, click _**New Product**_ button.

6. You'll be directed to new page then, at **Product Details** section, fill in all the required fields:
	- **UPC**- or **Universal Product Code**, this is very useful in creating barcode symbol.
	- **Parent**- applies to product that has various versions (i.e. sizes)
	- **Title**- name of product.
	- **Description**- refers to product description. This is the right venue to describe product origin, materials used to build the product, what inspires the designer to create such product and alike statements. **Important Note:** Refrain from copy pasting from web straight to this section, this results to rendering of style copied from online. To avoid this, paste your copied words to a text editor tool first, for Mac users use _**TextEdit**_. From a text editor tool, copy the words and then you may now paste it on this section.
	- **Is Discountable?**- this means you're allowing your product to participate on promotions that the site has to offer. If you think this applies to your product then toggle this option.
	- **Is Item one of a kind?**- this applies to rare products with limited quantity like collector's item. When you toggle this one, quantity display on product page will be then removed.
	- **Item Number**- unique numerical identifier of product.
	- **Width**, **Depth**, **Height** and **Weight**- displays the dimension and weight of the product. 
	- **Disabled**- this enables you to make your product inactive, applies to old product with least demand from market. The effect of this option is that it will hide your product on main site, just hidden not deleted. Under **Dashboard** menu, **Catalogue** then **Products**, a new tab named **Disabled Products** will be added to make room for this type of product. 
	- **Priority**- this is the section where you can manage the order of your products. Most likely, this applies to Veritas Greeting Cards. Currently it was set by default to alphabetical order but you may manually arrange the orders of it through this option. As what the guide says, products are displayed by priority--a higher number giving higher priority. For example, you have 3 products namely: A, B and C. Currently, when this option isn't used or with 0 value, page will automatically display the said 3 products in alphabetical order but you may display these 3 in reverse order (C B A instead of A B C) if you want to. To do so, set the priority of C to 3, B to 2 and A to 1. The one with the higher number displays first while the least is placed lastly. <br></br>

	![Product-Details](/img/p4.png) 
	
	**Note** - Copy pasting information to **Product Description** will result in the text styling being copied too. Please use plain text paste (**CTRL+SHIFT+V** using Windows or **CMD+SHIFT+V** using Mac) if pasting into the product description.

	![Product-Details](/img/p5.png) <br></br>

7. At **Product Category**, select one category from the list. If none, refer to [Adding a new Category](./category.md#adding-a-new-category) and learn how to create a new one.

	![Product-Category](/img/p6.png) <br></br>

8. At **Product Images**, click _**Upload Image**_ and locate the photo of your product that you want to be posted.

	![Product-Image](/img/p7.png) <br></br>

9. At **Product Stock**, fill in all the required fields:
	- **Partner**- refers to the source of your product. Simply choose one from the choices, if none refer to [Adding a New Partner](./partner.md#adding-a-new-partner) guideline and learn how to create one.
	- **SKU**- or **Stock Keeping Unit**, this is a code that consists of letters, numbers or symbols that uniquely identifies the product. Normally used in inventory data management.
	- **Num in Stock**- refers to product count.
	- **Num allocated**-
	- **Cost Price**- product gross price (including tax), displayed on main product page.
	- **Price(excluding tax)**- product net price, displayed on product page- **Product Details** tab.
	- **Shipping Price**- shipping cost of product. <br></br>

	Optional fields:

	- **Low stock threshold**- this enable you to set an alert when product stock gets low and reached the number entered on this field. Alerts will automatically added on **Stock Alerts** table, as what described on [Managing Low Stock Alerts](./stock.md#managing-low-stock-alerts).
	- **Retail Price**- means recommended price of the product, this will serve as comparison of price only with existing similar products in the market. <br></br>

	![Product-Stock](/img/p8.png) <br></br>

10. Click _**Save**_. A message will appear to confirm that your product is created, it must now added under **Products** table and also visible on the main site.

	**Note:** Along with confirmation message are the two different useful buttons such as:

	- **Edit again**- if you missed something, click this one.
	- **View it on the site**- to scan the product on main site, click this one. br></br>



### Editing an Existing Product through Dashboard

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and select _**Products**_.

3. List of created products will display here. Choose one product that you wish to modify, click **Actions** and select _**Edit**_.

	![Edit1](/img/pe1.png) <br></br>

4. Go to section that you wish to edit.

	![Edit2](/img/pe2.png) <br></br>

5. Click _**Save**_ . Select **Save and continue editing** if you missed something or **Cancel** to return to the menu. <br></br>



### Editing an Existing Product through Site

1. Log in using Admin credentials.

2. Go to _**Shop**_ menu and locate the product that you wish to edit.

3. You will be directed to selected item's detailed page. Click _**Edit this product**_ link to proceed with updating the product.

	![main](/img/es.png) <br></br>

4. Go to section that you wish to edit.

	![Edit2](/img/pe2.png) <br></br>

5. Click _**Save**_ to finish. A message that says "Updated product'name'" must show up.

	**Note:** Recently edited items can be found here.

	![Recently Edit](/img/pe3.png) <br></br>



### Creating Products that has Attributes

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and select _**Products**_.

3. Choose one product that you wish to obtain an attribute, click **Actions** and select _**Create Product Attributes**_ button.

	![Attribute1](/img/pa1.png) <br></br>

4. Fill in the following fields:
	- **Product Attribute**- attribute title (ex. Author, Designer)
	- **Value**- value of the created attribute title (ex. Saba Jawda, Sarah Jawda) <br></br>

	![Attribute2](/img/pa2.png)

	**Note:** You can use the following buttons to;

	- add a new field.

		![add](/img/pa4.png)

	  	When clicked, a new set of fields will come out.

	  	![add](/img/pa5.png) <br></br>

	- delete existing field.selected field will grey out once clicked.

		![delete](/img/pa3.png)

		When clicked, selected set of fields will grey out and will totally removed after clicking the **Submit** button.

		![add](/img/pa6.png) <br></br>

5. Click _**Submit**_ to Save.

	Entered attributes will display on **Product Details** tab, just like this.

	![attribute](/img/pa7.png) <br></br>


### Creating Products that has Different Colors or Sizes or Variants

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and select _**Products**_.

3. Choose one product that you wish to obtain product variant. Click its **Actions** button and select _**Create Product Variants**_.

	![Variant1](/img/pv1.png) <br></br>


4. Fill in _**Option Group**_ and its _**Options**_. You can add a thumbnail per option by clicking **Choose File**, a new window will come out, locate the image and then upload it afterwards. Be advised that **Delete** (trash icon), **View** (camera icon) and **Add** (plus sign icon) buttons are present on this page.

	- _**Option Group**_- product attribute title. (ex. Fabric, Finish)
	- _**Option**_- value of created option group. (ex. Cotton, Charcoal) <br></br>

	![Variant2](/img/pv2.png)

	_**Note:**_	You can save an option even without a thumbnail or image, thus it will display on the site with its given option name. See sample below:

	![Variant2](/img/pv3.png) <br></br>

5. Click _**Submit**_ to Save.

	Successful entry of product variants will display on product's main page, see below:

	![Variant2](/img/pv4.png) <br></br>



### Uploading the Thumbnail of Product Variant 

Thumbnails of product variant displays on product page when images are provided during the creation of variant. In case images are not uploaded during the process, a label (single option) or a selection box (multiple option) will appear on product page instead. There is no need to perform again the method of adding new product to make it visible, uploading a thumbnail on its parent product will do the work. This procedure is quite similar with the process described in [Creating Products that has different Colors or Sizes](./product.md#creating-products-that-has-different-colors-or-sizes). To start, simply follow the step by step procedure below:

1. Log in using admin credentials. <br></br>

2. Click _**Dashboard**_, _**Catalogue**_ and select _**Products**_. <br></br>

3. At the end of the page, you'll find the **Products** table where all products along with their details are listed. Select the product with present variant that you wish to have an image. Click **Actions** button and select _**Edit Product Variants**_.

	![th](/img/th1.png) <br></br>

4. You are now directed to **Update Product Variants for (name of product)** section. Go to your concerned option that needs an image. Below the option name, you will find the **Choose file** button. Click that button and then upload your preferred image. I will give you an example, let's try to add image on _**AZTEC**_ option. 


	![th](/img/th2.png) <br></br>

	After I uploaded the image, thumbnail box is now filled of the uploaded image. Same process applies to other variants that you wish to be presented by a thumbnail.


	![th](/img/th3.png) <br></br>

5. To save everything, just click the **Submit** button located at the lower section of the page. After I provide images for _**AZTEC**_, _**CHEVRON**_, _**WOOD**_ and _**GREY**_ from their respective groups, here's what it looks like now on product page.

	![th](/img/th4.png) <br></br>



### Updating the Details of an Existing Product Variant


During the creation of product variants, each entered options has its own section or page where you can modify the attributes if needed. This is the same page that you will encounter in [Adding a New Product](./product.md#adding-a-new-product) but with few additions. Let me guide you thoroughly, steps are as follows:

1. First, log in using admin credential. <br></br>

2. Click _**Dashboard**_, _**Catalogue**_ and select _**Products**_. <br></br>

3. From the **Products** table, select the parent product of option that needs an update. That can be a price change, update of posted image and the like. Click its **Actions** button and select _**Edit**_. As an example, I will try to edit the **Logo Lounge Chair**.


	![va](/img/va1.png) <br></br>

4. You are now facing the sections of parent product or of _**Logo Lounge Chair**_. Intended change to certain variant shouldn't performed here but on its separate section. 

	Now click **Product Variants** section. This sections displays the variants added on parent product. _**Logo Lounge Chair**_ has two child products (jawline-walnut & jawline-charcoal), just like what shown here:


	![va](/img/va2.png) <br></br>

5. As what I have mentioned on intro, variants present on parent product has its own page with sections as well. Now, let us try to get there. From the list, select one child product and click **Edit**. Let's edit the image and price of second child product (_**fabric**_: jawline; _**finish**_: charcoal).

	On **Product Attributes** section, you'll see the attribute description of selected variant. 


	![va](/img/va3.png) <br></br>

	On **Product Images** section, the best photo that you should upload here is the end product of mixed options from different option group (i.e. a chair with a jawline fabric and charcoal finish).


	![va](/img/va4.png) <br></br>

	On **Product Stock** section, you may freely update the price of a variant whenever you want. 


	![va](/img/va5.png) <br></br>

	![va](/img/va6.png) <br></br>


	Now, let me show you how those updated information displayed on main site. At first glimpse, with no clicked thumbnails or options yet, here's what the _**Logo Lounge Chair**_ product page looks like. Prices of both options are displayed in a price range manner, displayed item number is from the parent product and images are all displayed as well. **Note:** Image of parent product is always displayed no matter what.


	![va](/img/va7.png) <br></br>

	Let us now select each of the available options. Let's click the fabric _**Jawline**_ and the finish _**Walnut**_. Observe what changed on product page. Displayed price, image and item number is the selected option only while thumbnail of unselected option greyed out.


	![va](/img/va8.png) <br></br>

	Same goes with fabric _**Jawline**_ and finish _**Charcoal**_ when selected.


	![va](/img/va9.png) <br></br>

### Delete Variant Picture

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and then locate _**Products**_.

3. Search Title of the Product you want to **Delete** variants picture.

	![va](/img/delvar.png) <br></br>

4. Click _**Search**_.

	![va](/img/delvar1.png) <br></br>

	You will be directed to this page.

	![va](/img/delvar2.png) <br></br>

5. Look for the _**Parent**_ product, click **Actions**.

	![va](/img/delvar3.png) <br></br>

6. Click _**Edit Product Variants**_.

	![va](/img/delvar4.png) <br></br>

	You will be directed to this page.

	![va](/img/delvar5.png) <br></br>

7. Click _**Delete**_ box below the picture you want to delete. 

	![va](/img/delvar6.png) <br></br>	

8. Click _**Submit**_ to delete the picture.

	![va](/img/delvar7.png) <br></br>	

	You will be directed to this page.

	![va](/img/delvar8.png) <br></br>

**NOTE** - You can only delete variant images with this option, **Product Variant Edit > Product Images** will not let you delete variant images but will let you upload additional images.


### Viewing the Product on Site

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and then locate _**Products**_.

3. From the menu, select the product that you wanted to see on the site. Just click **Actions**, then select _**View on site**_.

	![View](/img/vp1.png) <br></br>

 	**Note:** While editing product, you may also view the made changes on site by clicking the _**View on site**_ button located at the bottom, left side hand of the page.

 	![View](/img/vp2.png) <br></br>



### Deleting an Existing Product

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and select _**Products**_.

3. From the menu, choose the product that you wish to delete. Click **Actions** and select _**Delete**_.

	![Delete](/img/dp1.png) <br></br>

4. A delete confirmation message will come out to validate your action. Here's what it looks like:

	![Delete2](/img/dp2.png) <br></br>

5. Click _**Delete**_ to continue or _**Cancel**_ to return to the menu. <br></br>



### Removing Quantity Option on Certain Products

1. Log in using Admin credentials.

2. In order to remove or hide the quantity option of a product, simply toggle the _**Is item one of a kind?**_ from **Product details** section.

	![qty](/img/qty1.png)

	You can access this option in two different ways:

	- **first** is from the time you are adding new product, if you are pretty sure that the product has limited piece then check this option right away. Follow the steps 1 to 6 from [Adding a New Product](./product.md#adding-a-new-product) and then enable the 'one of a kind' option.
	- **second** is by editing an existing product either through the main site or dashboard, you can check any of these two instructions til you locate the **Product details** section: [Editing an Existing Product through Dashboard](./product.md#editing-an-existing-product-through-dashboard) or [Editing an Existing Product through Site](./product.md#editing-an-existing-product-through-site). Enable the _**Is item one of a kind?**_ and click Save.

3. After enabling this option, display of quantity will be removed just like this...

	![qty](/img/qty2.png) <br></br>

4. At Shopping Bag page, one of a kind items is no longer editable. In case that customer changed their mind and decide not to buy the item, they can simply click **Remove**.

	![qty](/img/qty3.png) <br></br>




### Changing the Product Pricing

The easiest way to change the product pricing is through main site. You need to sign in using Admin account, browse the product from **Shop** menu and click _**Edit this product**_ link.

![price](/img/cost4.png)

Update the _**Cost Price**_ (gross price, including tax) and _**Price**_ (net price, excluding tax) under **Product Stock** section and click **Save**. Another way to change the price of a product is through **Dashboard**, steps are as follows:

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and then select _**Products**_.

3. Go to **Products** table, tab should be placed in **All** tab to show all created products.

	![price](/img/cost1.png) <br></br>

4. Locate the product that you wish to change the price, click **Actions** and select _**Edit**_.

	![price](/img/cost2.png) <br></br>

5. Proceed to **Product Stock** section and update the _**Cost Price**_ (gross price, including tax) and _**Price**_ (net price, excluding tax).

	![price](/img/cost3.png) <br></br>

6. Click **Save** to continue. A confirmation message will appear that says selected product is now updated, to check your made changes, click _**View it on the site**_ button and inspect the product price.


### Edit Product Variant Price

1. Log in using Admin credentials.

2. Click _**Dashboard**_, _**Catalogue**_ and then locate _**Products**_.

3. Search Title of the Product you want to **Edit Variant Price** of.

	![va](/img/delvar.png) <br></br>

4. Click _**Search**_.

	![va](/img/delvar1.png) <br></br>

	You will be directed to this page.

	![va](/img/delvar2.png) <br></br>

5. Click _**Actions**_ beside the product variant you want to edit price of.

	![va](/img/editvarprice.png) <br></br>

6. Click _**Edit**_.

	![va](/img/editvarprice1.png) <br></br>

	You will be directed to this page.

	![va](/img/editvarprice2.png) <br></br>

7. Click _**Product Stock**_ to be able to edit the product price.

	![va](/img/editvarprice3.png) <br></br>

	You will be directed to this page.

	![va](/img/editvarprice4.png) <br></br>

8. This is where you can edit the price of your variant.

	![va](/img/editvarprice5.png) <br></br>

	**NOTE** : This section is normally set manually.

	* **Partner** - Always set to Internal Stock.

	* **SKU** - You will define this according to what SKU you want to set.

	* **Price excl tax** - The Price you will put in here will be the Price that will be displayed on your website.	

	* **Shipping Price** - Shipping fee for the product variant itself, you can set 0 if it is for free.

	<br></br>

9. Click _**Save**_ to save the changes.

 	![va](/img/editvarprice6.png) <br></br>

 	You will be directed to this page.

 	![va](/img/editvarprice7.png) <br></br>

 	Click **View it on the site** to check how it looks on your website.

 	![va](/img/editvarprice8.png) <br></br>

 	You will be directed to this site.

 	![va](/img/editvarprice9.png) <br></br>