
### Adding a New Template to Plugins

1. Log in using Admin credentials.

2. Go to _**About**_ page.

3. Click _**Live**_ to edit the page then tick _**Structures**_.

4. Two plugins will came out, the **Left** with a picture template and **Right** plugins with a text.

	![ab](/img/ab1.png) <br></br>

5. To add a new template, simply move your cursor over either **Left** or **Right** plugin. As an example, I will select Right plugin. Under **Generic** are the list of templates that you can use to display the content of your page. I suggest the following 2 useful templates:

	- **Picture**- holds the image of your page
	- **Text**- displays the message of your page
	- **Link**- has the ability to direct you to web address

	![ab](/img/ab5.png) <br></br>

6. Once you finish to select a template and enter inputs to it, don't forget to click _**Save**_ and _**Publish Changes**_ to succesfully update the page. <br></br>

	**Note:** Active or existing template must be deleted first before you select a new one to avoid overlapping of its components.


### Changing the Picture on Left Plugin

1. Log in using Admin credentials.

2. Go to _**About**_ page.
n
3. Click _**Live**_ to edit the page then tick _**Structures**_.

4. Two plugins will came out, the **Left** with a picture template and **Right** plugins with a text.

	![ab](/img/ab1.png) <br></br>

5. To change the picture, hover over the menu of **Picture** template and select _**Edit**_.

	![ab](/img/ab2.png) <br></br>

6. Picture window will pop out, click _**Choose File**_ and upload a new photo.

7. Click _**Save**_, wait til the page finish its loading. Click _**Publish Changes**_ to post your made changes. A confirmation message will appear and voila, image is now changed to new one.

	![ab](/img/ab3.png) <br></br>



### Updating the Text on Right Plugin

1. Log in using Admin credentials.

2. Go to _**About**_ page.

3. Click _**Live**_ to edit the page then tick _**Structures**_.

4. Two plugins will came out, the **Left** with a picture template and **Right** plugins with a text.

	![ab](/img/ab1.png) <br></br>

5. To update the text, hover over the menu of **Text** template and select _**Edit**_.

6. Text window will pop out, you may now update the text or message of this page.

	![ab](/img/ab4.png) <br></br>

7. Click _**Save**_, wait til the page finish its loading. Click _**Publish Changes**_ to post your made changes. A confirmation message will once again appear and text is now updated.




### Deleting an Existing Template

1. Log in using Admin credentials.

2. Go to _**About**_ page.

3. Click _**Live**_ to edit the page then tick _**Structures**_.

4. Two plugins will came out, the **Left** with a picture template and **Right** plugins with a text.

	![ab](/img/ab1.png) <br></br>

5. To delete an active template, move your cursor over either **Picture** or **Text** template and select _**Delete**_. As an example, let's delete the Text template.

	![ab](/img/ab6.png) <br></br>

6. A message will pop out to validate your action, click _**Yes, I'm sure**_ to continue.

7. The page will load for a while, wait to finish. Under **Right** plugin, you can notice that Text template are no longer there and that means you successfully removed the template.

8. To publish the deletion you made, click the _**Publish Changes**_ button. As the page reloads, a confirmation message will appear and you can see that right section of your page is now blank and empty.

	![ab](/img/ab7.png) <br></br>
