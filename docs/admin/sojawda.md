
### Adding a New Category

1. Log in using Admin credentials.

2. Enable the editing mode of the page, click _**Live**_ button.

3. Click _**Zinnia**_ and select _**New Category**_.

4. Create a new category, minimum requirement is _**Title**_. Let's try to add a new category, as an example i will add **Spotlight:Shoes**.

	![sb](/img/sb2.png) <br></br>

5. Click _**Save**_ to continue. A 'added' confirmation message of your action will prompt at the top left hand of the page. Newly added category is now also listed along with other active categories.

	![sb](/img/sb9.png) <br></br>


	**Note:** Newly created category won't be display on the site until you create an entry to it. <br></br>



### Adding a New Entry

1. Log in using Admin credentials.

2. Enable the editing mode of the page, click _**Live**_ button.

3. Click _**Zinnia**_ and select _**New Entry**_.

4. Create a new entry, important fields are as follows:
	- **Title**- name of entry
	- **Status**- there are three selections:
		- **draft**- preliminary version of an entry.
		- **hidden**- temporarily not visible on main site.
		- **published**- displayed on main site, always select this to publish your made changes.
	- **Content**- message of your entry
	- **Image**- displayed photo of your entry
	- **Categories**- designation of your entry <br></br>

	![sb](/img/sb3.png) <br></br>

	There are two ways to select from the list of **available categories**: first, select one category and click the highlighted arrow icon or; second and the simplest way, double click the category and voila it is now placed to your **chosen categories** box. <br></br>

	![sb](/img/sb4.png) <br></br>

	As an example, I will add a new entry under **Spotlight:Shoes** named _Louboutin_. <br></br>

5. Click _**Save**_, a confirmation message must display that says _"The entry 'Louboutin:published' was added successfully"_ and newly created entry must now added in the list along with other active entries. To check it on the site, close the cms window and go to Sojawda Blog page, refresh the page and boom **Louboutin** is now published (see screenshot below).

	![sb](/img/sb10.png) <br></br>



### Viewing the List of Categories

1. Log in using Admin credentials.

2. Enable the editing mode of the page, click _**Live**_ button.

3. Click _**Zinnia**_ and select _**Categories List**_.

4. Informations of all active categories are all listed here, option of editing or deleting a category can be found here as well. Don't forget to expand the page by clicking the small square icon.

	![sb](/img/sb6.png) <br></br>

5. To edit, simply click the name itself and it will direct you to its content. Once done, click _**Save**_. A confirmation message will display that says 'The category (name of category) was changed successfully' and newly added information are now displayed in the table.

	![sb](/img/sb11.png) <br></br>

6. To delete, check the category that you want to remove. From action list, select **Delete selected entries** and click **Go**. Just say yes to delete confirmation message and voila, category is now removed from the list as you refresh your page.

	![sb](/img/sb5.png) <br></br>




### Viewing the List of Entries

1. Log in using Admin credentials.

2. Enable the editing mode of the page, click _**Live**_ button.

3. Click _**Zinnia**_ and select _**Entries List**_.

4. Informations of all active entries are all listed here, option of editing or deleting an entry can be found here as well. To view the list in full page, click the small square icon.

	![sb](/img/sb1.png) <br></br>

5. To edit, simply click the entry name itself and it will direct you to its content. Once done, click _**Save**_. A confirmation message will display that says 'The category (name of entry) was changed successfully' and newly added information are now displayed in the table.

	![sb](/img/sb7.png) <br></br>

6. To delete, check the entry that you want to remove. From action list, select **Delete selected entries** and click **Go**. Just say yes to delete confirmation message and voila, entry is now removed from the list as you refresh your page.

	![sb](/img/sb8.png) <br></br>

	**Note:** There are other options under **Action** list that you can use if necessary. <br></br>